import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class creates the balloon, drop it at the speed that was set from MyWorld
 * When the balloon drops to the bottom of the map, it jumps to the game over screen
 * 
 * @Charmaine, Gary, Andy
 * April 2018
 */
public class Balloon extends Actor
{
    public void act(){
        MyWorld world = (MyWorld) getWorld();
        if (((MyWorld) getWorld()).drop)
        {            
            setLocation(getX(), getY()+((MyWorld) getWorld()).dropSpeed);
            //drop the balloon after when certain amount of time
        }
        if (getY() == getWorld().getHeight()-1 ) {
            int i = ((MyWorld) getWorld()).score.currentScore();
            ((MyWorld) getWorld()).exist=false;
            getWorld().removeObject(this);
            Greenfoot.setWorld(new GameOver(i));
            GreenfootSound gg = new GreenfootSound("Good Game.mp3");
            gg.play();
            //once the balloon drop out of the map, the game is over
        }
    }
}
