import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class creates the text the baby says
 * 
 * @Charmaine, Andy, Gary
 * April 2018
 */
public class CryCloud extends Actor
{
    /**
     * Act - do whatever the CryCloud wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
