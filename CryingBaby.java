import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the image of the crying baby
 * 
 * Gary, Charmaine, Andy 
 * April 2018
 */
public class CryingBaby extends Actor
{
    /**
     * Act - do whatever the CryingBaby wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
