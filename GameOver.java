import greenfoot.*; 
import java.util.*; 
/**
 * This is this the game over screen world 
 * 
 * @By: Andy, Gary, Charmaine 
 * @ April 2018
 */
public class GameOver extends World
{
    HashMap<Integer,String> result = new HashMap<Integer,String>();
    public int finalPoint = 0;
    public GameOver(int score)
    {          
        super(600, 400, 1); 
        for (int i=1; i < 15; i ++)
        {
            result.put(i,"You can do better!!!");
        }
        for (int i=15; i < 25; i ++)
        {
            result.put(i,"No bad!!!");
        }
        for (int i=25; i < 35; i ++)
        {
            result.put(i,"Great Job!!!");
        }
        result.put(35,"WOW! Amazing!!!");
        finalPoint = score;
        CryingBaby cryingbaby;
        cryingbaby = new CryingBaby();
        addObject(cryingbaby,300, 350);
        CryCloud crycloud;
        crycloud = new CryCloud();
        addObject(crycloud,450, 220);
        GoHome home = new GoHome();
        addObject(home,535,335);
        int item = 0;
        if (finalPoint < 1)
        {
            item = 1;
        }
        if (finalPoint >= 35)
        {
            item = 35;
        }
        GameOverDisplay gameover = new GameOverDisplay(result.get(item));
        addObject(gameover,300,80);
        ResultDisplay resultDisplay = new ResultDisplay(score);
        addObject(resultDisplay,300,120);

    }
}
