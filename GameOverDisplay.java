import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class creates the text for the game over screen
 * 
 * @Charmaine, Gary, Andy 
 * April 2018
 */
public class GameOverDisplay extends Actor
{
    /**
     * Act - do whatever the GameOverDisplay wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public String conclusion = "";
    public GameOverDisplay (String result)
    {
        conclusion = result;
    }
    public void act() 
    {
        setImage (new GreenfootImage(conclusion, 45, Color.YELLOW, new Color(0,0,0,0)));
    }    
}
