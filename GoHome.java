import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the home button for the user to press to return to the home screen
 * 
 * @Charmaine, Andy, Gary
 * April 2018
 */
public class GoHome extends Actor
{
    /**
     * Act - do whatever the GoHome wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public GoHome(){
        GreenfootImage image=getImage();
        image.scale(60,60);
        setImage(image);
    }
    public void act() 
    {
        if(Greenfoot.mouseClicked(this))
        {
            Greenfoot.setWorld(new TitleScreen());
            // allows the player to return to the Title screen
        }
    }
}