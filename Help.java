import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * This is the class for the get help button which opens up the help screen if pressed
 * 
 * @By: Andy Gary Charmaine 
 * @ April 2018
 */
public class Help extends Actor
{
    /**
     * Act - do whatever the Help wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed ina the environment.
     */
    public void act() 
    {
        if(Greenfoot.mouseClicked(this))
        {
            Greenfoot.setWorld(new HelpScreen());
            // jump to the help screen whenever the user click on the button
        }
    }    
}
