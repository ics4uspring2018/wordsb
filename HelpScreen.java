import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * This is the help screen world which has the instructions of the game
 * 
 * @By: Andy, Charmaine, Gary 
 * @April 2018
 */
public class HelpScreen extends World
{
    /**
     * Constructor for objects of class HelpScreen.
     * 
     */
    public HelpScreen()
    {    
        super(600, 400, 1); 
        GoHome home = new GoHome();
        addObject(home,535,335);
        // Created a home object so that you can return back to the main screen 
    }
}