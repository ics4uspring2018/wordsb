import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class displays what level the user is at
 * 
 * @Charmaine, Andy, Gary
 * April 2018
 */
public class LevelDisplay extends Actor
{
    /**
     * Act - do whatever the LevelDisplay wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public int level = 1;
    public LevelDisplay()
    {
        setImage (new GreenfootImage("Level " + level , 30, Color.BLACK, new Color(0,0,0,0)));
    }
    public void display() 
    {
        MyWorld world = (MyWorld) getWorld();
        if (((MyWorld) getWorld()).score.currentScore()%5==0 && level < 7)
        {
            level ++;
        }
        // the max level is 6 and the player levels up for every 5 correct word
        setImage (new GreenfootImage("Level " + level , 30, Color.BLACK, new Color(0,0,0,0)));
    }    
}
