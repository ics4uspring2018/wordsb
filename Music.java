import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the music button that connects to the music options
 * 
 * Andy Gary Charmaine
 * April 2018
 */
public class Music extends Actor
{
    /**
     * Act - do whatever the Music wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public Music()
    {
        GreenfootImage image=getImage();
        image.scale(80,80);
        setImage(image);
    }
     public void act() 
    {
        if(Greenfoot.mouseClicked(this))
        {
            Greenfoot.setWorld(new MusicWorld());
        }
    }
}
