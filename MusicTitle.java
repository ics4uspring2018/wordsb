import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the title of the MusicWorld screen
 * 
 * Andy Gary Charmaine
 * April 2018
 */
public class MusicTitle extends Actor
{
   
    /**
     * Act - do whatever the Text wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public MusicTitle(){ 
        setImage(new GreenfootImage("Pick a track:", 40, Color.BLACK,Color.WHITE));
       
    }
    public void act() 
    {
        // Add your action code here.
    }    
}
