import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.HashMap;
/**
 * This is the music world of the program
 * 
 * @By: Andy Gary Charmaine 
 * @ April 2018
 */
public class MusicWorld extends World
{
    HashMap<Integer,String> music = new HashMap<Integer,String>();  // Created a hashmap to store each song
    ChineseMusic chinesemusic = new ChineseMusic();
    SunnyMusic sunnymusic = new SunnyMusic();   
    FireMusic firemusic = new FireMusic();
    /**
     * Constructor for objects of class MusicWorld.
     * 
     */
    public boolean playsong1 = false;
    public boolean playsong2 = false;
    public boolean playsong3 = false;
    GreenfootSound song1 = new GreenfootSound("Chinese.mp3");  
    GreenfootSound song2 = new GreenfootSound("Sunny.mp3");  
    GreenfootSound song3 = new GreenfootSound("Fire.mp3");
    public MusicWorld()
    {    
       super(600, 400, 1); 
       GoHome home = new GoHome();
       addObject(home,550,50);   
       MusicTitle title= new MusicTitle();
       addObject(title,120,50);
       addObject(chinesemusic,300,300);
       addObject(sunnymusic,300,200);
       addObject(firemusic,300,100);
    }

    public void act()
    {
       
       if(Greenfoot.mouseClicked(chinesemusic))
        {
            if (playsong1)
            {  
            }
            else
            {
                song2.stop();
                song3.stop();
                song1.play();
                playsong2 = false;
                playsong3 = false;
            }
            playsong1 = true;
        }
        else if (Greenfoot.mouseClicked(sunnymusic))
        {
            if (playsong2)
            {  
            }
            else
            {
                song1.stop();
                song3.stop();
                song2.play();
                playsong1 = false;
                playsong3 = false;
            }
            playsong2 = true;
        }
        else if (Greenfoot.mouseClicked(firemusic))
        {
            if (playsong3)
            {  
            }
            else
            {
                song1.stop();
                song2.stop();
                song3.play();
                playsong1 = false;
                playsong3 = false;
            }
            playsong3 = true;
        }
    }
}
