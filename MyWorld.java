import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
/**
 * This is the main game world 
 * 
 * @Gary Charmaine Andy 
 * @April 2018
 */
public class MyWorld extends World
{
    public RandomText randomtext;
    public Random rand = new Random(); 
    public Typing typing;
    public Score score;
    public LevelDisplay levelDisplay;
    public Baby baby;
    public WordCloud wordCloud;
    public boolean drop = false;
    public int time = 0;
    public int dropSpeed = 1;
    public boolean exist=false;
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        prepare();
    }
    private void prepare()
    {
        randomtext = new RandomText();       
        addObject(randomtext, 300, 80);
        typing = new Typing();
        addObject(typing,286,255);
        score = new Score();
        addObject(score,100,355);
        levelDisplay = new LevelDisplay();
        addObject(levelDisplay,100, 330);
        baby = new Baby();
        addObject(baby,560, 340);
        wordCloud = new WordCloud();
        addObject(wordCloud,530, 180);
    }
    public void act()
    {
        time += 5;
        if (time % 15 == 0)
        {
            drop = true;
        }
        else
        {
            drop = false;
        }
        if (time % 1500 == 0 && dropSpeed < 12)
        {
            dropSpeed += 1;
        }
        //drop speed increase over time
        if (exist == false)
        { 
            balloon(); 
        }
    }
    public void balloon()
      {
        //int i = rand.nextInt(130);      
        //if (i == 1) {
        Balloon balloon = new Balloon();
        addObject(balloon,Greenfoot.getRandomNumber(400)+100, 0);// places the asteroid at a random x location
        exist = true;
        // add balloon when only when there isn't any
     }
}
