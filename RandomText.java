import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import java.util.Scanner;
import java.io.InputStream;
/**
 * This is the class that creates the random word for the user to type, it takes a words from the over
 * 3000 words text files, there are 6 words files with different word length, which will be selected 
 * based on the level of the game, and level is based on the score of the player
 * 
 * @Charmaine Gary Andy 
 * @April 2018
 */
public class RandomText extends Actor
{   
    ArrayList<String> list = new ArrayList<String>();
    int current = 0;
    int level = 1;
    public RandomText()
    {
        String wordlevel = "level ";
        wordlevel += Integer.toString(level);
        Scanner input = getScanner(wordlevel);
        while (input.hasNextLine())
        {
            list.add (input.nextLine());
        }
        //use an arraylist to store the words
        int i = Greenfoot.getRandomNumber(list.size()); 
        current = i;
        setImage (new GreenfootImage(list.get(i), 50, Color.BLACK, new Color(0,0,0,0)));
        //display the chosen random word
    }   
    public void newRandomWord()
    {
        MyWorld world = (MyWorld) getWorld();
        if (((MyWorld) getWorld()).score.currentScore()%5==0)
        {
            list.clear();
            if (level < 6 )
            {
                level++;
            }
            // for every 5 point, the level will increase by 1, in other words, the length of the 
            //word will increase by 1
            String wordlevel = "level ";
            wordlevel += Integer.toString(level);
            Scanner input = getScanner(wordlevel);
            while (input.hasNextLine())
            {
                list.add (input.nextLine());
            }
            // new lists will be made once the player reaches a higher level
        }
        int i = Greenfoot.getRandomNumber(list.size());
        current = i;
        setImage (new GreenfootImage(list.get(i), 50, Color.BLACK, new Color(0,0,0,0)));
        //reset the display to the new word
    }   
    public String getCurrentWord()
    {
        return list.get(current);
    }
    public Scanner getScanner (String filename){
        InputStream myFile = getClass().getResourceAsStream(filename);
        if (myFile != null)
        {
            return new Scanner(myFile);
        }
        //scanner used for accessing the words files
        return null;
    }
    
}