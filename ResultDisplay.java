import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This displays the final score of the player when the game is finished
 * 
 * Gary, Charmaine, Andy 
 * April 2018
 */
public class ResultDisplay extends Actor
{
    /**
     * Act - do whatever the resultDisplay wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public ResultDisplay (int point)
    {
        int score = point;
    }
    public void act() 
    {
        GameOver world = (GameOver)(getWorld());
        int result = world.finalPoint;
        setImage (new GreenfootImage("Your Score is " + result, 45, Color.BLACK, new Color(0,0,0,0)));
        // Display the final score of the player
    }    
}
