import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * This is the Score class that creates the score image and also stores the score of the player
 * Andy Gary Charmaine
 * April 2018
 * 
 */
public class Score extends Actor
{
    public int score= 0 ;
    public void act() 
    {
        setImage (new GreenfootImage("Score: " + score, 30, Color.BLACK, new Color(0,0,0,0)));
    }    
    public void point()
    {
        score++;
    }
    public int currentScore()
    {
        return score;
    }
}

