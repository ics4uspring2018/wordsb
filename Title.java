import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the title class that displays our game's name
 * 
 * @Andy Gary Charmaine
 * @April 2018
 */
public class Title extends Actor
{    
    public Title(){ 
        setImage(new GreenfootImage("Baby Balloon Pop", 80, Color.WHITE,new Color(0,0,0,0)));     
        //display the game name
    }
    public void act() 
    {
        // Add your action code here.
    }    
}
