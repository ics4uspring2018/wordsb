import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * This is the titlescreen of the program, it is the first screen to appear
 * 
 * @By: Andy Gary Charmaine 
 * @ April 2018
 */
public class TitleScreen extends World
{
    /**
     * This is the title screen world that the user first sees when they open the game
     * 
     * 
     */
    public TitleScreen()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        prepare();
        
    }
    

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        Title header = new Title(); 
        Title text = new Title();
        addObject(text,294,201);
        text.setLocation(303,104);
        Help help = new Help();
        addObject(help,300,328);
        Play play = new Play();
        addObject(play,301,305);
        play.setLocation(300,274);
        Music music = new Music();
        addObject(music,493,76);
        music.setLocation(543,343);
    }
}
