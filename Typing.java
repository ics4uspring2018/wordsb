import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * This is class allows the user to type the word to match the text, if the user inputs the correct word,
 * it calls the method in RandomText to regenerate new word
 * 
 * @By: Andy, Charmaine, Gary 
 * @April 2018
 */
public class Typing extends Actor
{
    String word = "";
    RandomText randomtext;
    Score score;
    public Typing()
    {
        setImage( new GreenfootImage("", 20, Color.BLACK, new Color(0,0,0,0)));
    }
    public void act() 
    {
        checkKey();
        setImage (new GreenfootImage(word, 40, Color.BLACK, new Color(0,0,0,0)));
    }    
    public void checkKey()
    {
        String key = Greenfoot.getKey();
        MyWorld world = (MyWorld) getWorld();
        if (key == "backspace")
        {
            if (word.length() > 0)
            {
                word = word.substring(0, word.length()-1);
                setImage (new GreenfootImage(word, 40, Color.BLACK, new Color(0,0,0,0)));
                // backspace only when something has already been entered
            }
        }
        else if (key == "enter" ||key == "up"||key == "down"||key == "left" ||key == "right"||key == "space" || key == "shift" || key == "control")
        {
            word = word + "";
            //when player press the arrows keys, enter keys, etc. nothing is added
        }

        else if (key != null)
        {
            word = word + key; 
            if (word.equals(((MyWorld)(getWorld())).randomtext.getCurrentWord()))
            {
                ((MyWorld)(getWorld())).score.point();
                word = "";
                //refresh the textbox
                GreenfootSound pop = new GreenfootSound("pop.aiff");
                pop.play();
                //set a new random word
                ((MyWorld)(getWorld())).randomtext.newRandomWord(); 
                ((MyWorld)(getWorld())).levelDisplay.display(); 
                ((MyWorld) getWorld()).exist = false;
                getWorld().removeObjects(getWorld().getObjects(Balloon.class));
                //pop the balloon once the right word is entered
            }
        }
    }
}


